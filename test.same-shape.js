let a = {
    x: 1,
    y: 2,
    z: 3
};

let b = {};
b.x = 1;
b.y = 2;
b.z = 3;

let c = {
    x: 1,
    y: 2,
    z: 3
};

let d = {
    y: 2,
    z: 3,
    x: 1
};

console.log('a and b have same map:', % HaveSameMap(a, b));
console.log('a and c have same map:', % HaveSameMap(a, c));
console.log('a and d have same map:', % HaveSameMap(a, d));