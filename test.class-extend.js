class A {
    constructor(x, y) {
        this.x = x;
        this.y = y;
    }
}
class B extends A {
    constructor(x, y) {
        super(x, y);
    }
}
class C {
    constructor(x, y) {
        this.x = x;
        this.y = y;
    }
}
let a = new A(1, 2);
let a2 = new A(1);
let b = new B(1, 2);
let c = new C(1, 2);
console.log('a and a2 have same map', %HaveSameMap(a, a2));
console.log('a and b have same map:', %HaveSameMap(a, b));
console.log('a and c have same map:', %HaveSameMap(a, c));