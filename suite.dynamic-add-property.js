'use strict';
const benchmark = require('benchmark');
const suite = new benchmark.Suite();
class A {
  constructor (x, y, z, i) {
    this.x = x;
    this.y = y;
    this.z = z;
    this.i = i;
  }
}
function access(obj) {
  obj.x;
}
suite.add('literal add', function () {
  let temp = {
    x: 1,
    y: 2,
    z: 3
  };
  temp.a = 4;
  access(temp);
}).add('class', function () {
  let temp = new A(1, 2, 3, 4);
  access(temp);
}).add('origin literal', function () {
  let temp = {
    x: 1,
    y: 2,
    z: 3,
    a: 4
  };
  access(temp);
});
suite.on('complete', require('./print'));
suite.run();