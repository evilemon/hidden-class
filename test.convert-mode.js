let obj = {
    x: 1,
    y: 2
};

function FastOrDic(obj, str = '') {
    console.log(str, %HasFastProperties(obj) ? '开启FastMode' : '开启DicMode');
}

FastOrDic(obj, '新建对象');

delete obj.x;

FastOrDic(obj, 'delete一个对象属性之后');

%ToFastProperties(obj); // 强制优化转换为fast mode

FastOrDic(obj, '强制优化');

for (var i = 0; i < 100; i++) {
    obj['arg' + i] = i;
}

FastOrDic(obj, '动态添加大量属性');