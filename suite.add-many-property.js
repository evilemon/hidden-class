'use strict';
const benchmark = require('benchmark');
const suite = new benchmark.Suite();

class A {
    constructor (x, y) {
        this.x = x;
        this.y = y;
    }
}
function create(obj, counts) {
    for (let i = 0; i < counts; i++) {
        obj['i_' + i] = i;
    }
    return obj;
}
function test(obj, key, total = 1e4) {
    for(let i = 0; i < total; i++) {
        obj[key];
    }
}
let obj1 = create(new A(1, 2), 0);
let obj2 = create(new A(1, 2), 10);
let obj3 = create(new A(1, 2), 100);
let obj4 = create(new A(1, 2), 1000);
suite.add('add more 0 properties', function() {
    test(obj1, 'x');
}).add('add more 10 properties', function() {
    test(obj2, 'x');
}).add('add more 100 properties', function() {
    test(obj3, 'x');
}).add('add more 1000 properties', function() {
    test(obj4, 'x');
});
suite.on('complete', require('./print'));
suite.run();