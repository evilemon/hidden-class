function MagicFunc(obj) {
    function FakeConstructor() {
        this.x = 0;
    }
    FakeConstructor.prototype = obj;
    new FakeConstructor();
    new FakeConstructor();
};

function FastOrDic(obj, str = '') {
    console.log(str, % HasFastProperties(obj) ? '开启FastMode' : '开启DicMode');
}

let obj = {
    x: 1,
    y: 2
};

delete obj.x;

FastOrDic(obj, '删除属性')

MagicFunc(obj);

FastOrDic(obj, '设置原型')