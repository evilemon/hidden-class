let a = {
	x: 1,
	y: 2
};

let b = {
	x: 1,
	y: 4
};

b.z = 2;

console.log(a, b, %HaveSameMap(a, b));

delete b.z;

console.log(a, b, %HaveSameMap(a, b));