const v8 = require('v8-natives');

class A {
    constructor(a) {
        this.x = 1;
        this.y = 2;
        this.z = 4;
        this[a] = a;
    }
}
const max = 1e6;
console.log(v8.haveSameMap(new A(-3), new A(-3)));
console.log(v8.haveSameMap(new A(-1), new A(-2)));
let t = new Date();
for (let i = 0; i < max; i++) {
    new A(i).a;
}
console.log(`首次初始化${max}个对象，耗时${new Date - t}ms`);
t = new Date();
for (let i = 0; i < max; i++) {
    new A(i).a;
}
console.log(`第二次创建${max}个对象，耗时${new Date - t}ms`);
t = new Date();
for (let i = 0; i < max; i++) {
    new A(i).a;
}
console.log(`第三次创建${max}个对象，耗时${new Date - t}ms`);
