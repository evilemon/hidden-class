const v8 = require('v8-natives');
let obj = {
    a: 1,
    b: 2,
    c: 3
};

function FastOrDic(obj, str = '') {
    console.log(str, v8.hasFastProperties(obj) ? '开启FastMode' : '开启DicMode');
}
obj.d = 4;

delete obj.d;
FastOrDic(obj, '删除最后添加的属性');

delete obj.c;
FastOrDic(obj, '删除最后添加的属性');

delete obj.a;
FastOrDic(obj, '删除第一个添加的属性');