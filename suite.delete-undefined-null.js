'use strict';
const benchmark = require('benchmark');
const suite = new benchmark.Suite();
function access(obj) {
  obj.y;
}
class A {
  constructor(x, y) {
    this.x = x;
    this.y = y;
  }
}
suite.add('setting to undefined', function () {
  let obj = new A(2, 3);
  obj.x = undefined;
  access(obj);
}).add('setting to null', function () {
  let obj = new A(2, 3);
  obj.x = null;
  access(obj);
}).add('delete', function () {
  let obj = new A(2, 3);
  delete obj.x;
  access(obj);
}).add('setting to undefined literal', function undefPropLit () {
  let obj = { x: 2, y: 3 };
  obj.x = undefined;
  access(obj);
}).add('setting to null literal', function undefPropLit () {
  let obj = { x: 2, y: 3 };
  obj.x = null;
  access(obj);
}).add('delete property literal', function deletePropLit () {
  let obj = { x: 2, y: 3 };
  delete obj.x;
  access(obj);
});
suite.on('complete', require('./print'));
suite.run();