'use strict';
const benchmark = require('benchmark');
const suite = new benchmark.Suite();
function createObjects() {
    return [{ x: 1, y: 2, z: 3}, { a: 1, b: 2, c: 3}];
}
function test(obj) {
    let sum = 0;
    for (var i = 0; i < 100; i++) {
        sum += obj.a + obj.c;
    }
    return sum;
}
suite.add('case:with no delete', function () {
    var pair = createObjects();
    test(pair[1]);
}).add('case:delete but not access', function () {
    let pair = createObjects();
    delete pair[0].y;
    test(pair[1]);
}).add('case:delete and access', function () {
    let pair = createObjects();
    delete pair[1].b;
    test(pair[1]);
});
suite.on('complete', require('./print'));
suite.run();