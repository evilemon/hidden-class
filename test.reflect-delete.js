let a = {
    name: 'slm',
    age: 30
};

console.log(Object.getOwnPropertyDescriptors(a));

Object.defineProperty(a, 'name', {
    configurable: false
});

delete a.name;
Reflect.deleteProperty(a, 'age');

console.log(a);