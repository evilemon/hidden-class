'use strict';
const benchmark = require('benchmark');
const suite = new benchmark.Suite();
class A {
    constructor(x, y, z, a, b, c) {
        this.x = x;
        this.y = y;
        this.z = z;
        this.a = a;
        this.b = b;
        this.c = c;
    }
}
function access(obj) {
    obj.a;
}
suite.add('delete first property x', function deleteProp() {
    let obj = new A(1, 2, 3, 4, 5, 6);
    delete obj.x;
    access(obj);
}).add('delete third property z', function deleteProp() {
    let obj = new A(1, 2, 3, 4, 5, 6);
    delete obj.z;
    access(obj);
}).add('delete last property c', function deleteProp() {
    let obj = new A(1, 2, 3, 4, 5, 6);
    delete obj.c;
    access(obj);
}).add('no delete', function deleteProp() {
    let obj = new A(1, 2, 3, 4, 5, 6);
    access(obj);
});
suite.on('complete', require('./print'));
suite.run();